# Events Demo

## Backend
#### Technology used
1. Express
2. Node
3. Mongo & Mongoose
4. Jsonwebtoken
5. JOI Validation

#### To run
1. Goto backend folder
2. yarn install
3. node index.js


## Frontend
#### Technology Used
1. React
2. Redux
3. Redux Toolkit ( along with Thunk)
4. Material UI
5. CSS
6. Axios

#### To run
1. goto frontend folder
2. yarn install
3. yarn start
4. 


