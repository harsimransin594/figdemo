const jwt = require('jsonwebtoken');
const {httpErrorHandler} = require("../helper/http-handler");

const authMiddleware = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const jwtSecret = process.env.JWT_SECRET;
        console.log({token, jwtSecret});
        const decodedToken = jwt.verify(token, jwtSecret);
        next();
    } catch {
        return httpErrorHandler({res, status: 401, error:"Invalid authorization"})
    }
};

module.exports = {authMiddleware};