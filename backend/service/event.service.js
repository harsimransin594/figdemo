const eventModel = require("../model/event");
const {httpSuccessHandler, httpErrorHandler} = require("../helper/http-handler");
const PAGE_SIZE = 5;

const createEventService = async (res, value) => {
    try {
        const event = await eventModel.create(value);
        return httpSuccessHandler({res, data: {event}})
    }catch (e) {
        return httpErrorHandler({res, error: err.message});
    }
}

const getEventsService = async (res, value) => {
    try {
        const {page, ...filterParams} = value;
        for (let key in filterParams) {
            if (filterParams[key] === undefined || filterParams[key] === "") {
                delete filterParams[key];
            } else if (key !== "isVirtual")
                filterParams[key] = new RegExp(filterParams[key], "i")
        }
        const skipCount = (page - 1) * PAGE_SIZE
        const events = await eventModel.find(filterParams).limit(PAGE_SIZE).skip(skipCount);
        const pageCount = Math.ceil((await eventModel.find(filterParams).count()) / PAGE_SIZE);
        return httpSuccessHandler({res, data: {events, pageCount, page}})
    }catch (e) {
        return httpErrorHandler({res, error: e.message})
    }
}

const getCategoriesService = async (res) => {
    try {
        const categories = await eventModel.distinct("category")
        return httpSuccessHandler({res, data: {categories}})
    } catch (err) {
        return httpErrorHandler({res, error: err.message})
    }
}

module.exports = {createEventService, getEventsService, getCategoriesService}