const express = require("express");
const app = express();
const cors = require('cors')
require("dotenv").config();
const eventRouter = require("./router/event.router");
const mongooseConnection = require("./config/db");

const PORT = process.env.PORT || 8000;

app.use(express.json());
app.use(cors());
app.use("/api/event", eventRouter);


app.listen(PORT, () => {
    mongooseConnection()
    console.log(`server is successfully started at port ${PORT}`)
})

module.exports = app;