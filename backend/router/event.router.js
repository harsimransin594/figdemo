const express = require("express");
const eventModel = require("../model/event");
const {eventValidationSchema, filterSchema} = require("../validation/event.schems");
const {httpErrorHandler, httpSuccessHandler} = require("../helper/http-handler");
const {createEventService, getEventsService, getCategoriesService} = require("../service/event.service");
const events = require("events");
const {authMiddleware} = require("../middleware/auth.middleware");
const eventRouter = express.Router();

eventRouter.use(authMiddleware)

const createEvent = async (req, res) => {
        const {value, error} = await eventValidationSchema.validate(req.body);
        if (error) return httpErrorHandler({res, status: 400, error: error.message});
        return createEventService(res, value)

};

const getEvents = async (req, res) => {
        const {value, error} = await filterSchema.validate(req.query);
        if (error) return httpErrorHandler({res, status: 400, error: error.message});
        return  getEventsService(res, value)
};

async function getCategories(req, res) {
    return getCategoriesService(res)
}

eventRouter
    .route("/")
    .get(getEvents)
    .post(createEvent)


eventRouter
    .route("/category")
    .get(getCategories)

module.exports = eventRouter;