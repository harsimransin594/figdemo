const Joi = require('joi');

const eventValidationSchema = Joi.object({
    title: Joi.string().min(5).max(128).required(),
    description: Joi.string().min(10).max(1024).required(),
    category: Joi.string().min(2).max(64).required(),
    date: Joi.date().min("now").required(),
    isVirtual: Joi.boolean().required(),
    address: Joi.string().min(5).max(1024).required()
})

const filterSchema = Joi.object({
    title: Joi.string(),
    category: Joi.string(),
    isVirtual: Joi.boolean(),
    address: Joi.string(),
    page: Joi.number().default(1).min(1).default(1)
})

module.exports = {eventValidationSchema, filterSchema}
