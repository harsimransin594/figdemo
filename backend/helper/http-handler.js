const httpErrorHandler = ({ res, status = 500, error = "Internal Server Error" }) => {
    return res.status(status).json({ message: error })
}

const httpSuccessHandler = ({ res, status = 200, data }) => {
    return res.status(status).json({ data })
}

module.exports = { httpErrorHandler, httpSuccessHandler }