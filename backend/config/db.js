const mongoose = require("mongoose");
const mongooseConnection = () => {
    mongoose.connect(process.env.DB_LINK).then(function () {
        console.log("connected to db");
    }).catch((err) => {
        console.error("Error connecting to db", err)
    })
}

module.exports = mongooseConnection