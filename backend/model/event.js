const mongoose = require("mongoose");

const eventSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    isVirtual: {
        type: Boolean,
        default: true,
        required: true
    },
    address: {
        type: String,
        required: true
    }
})

const event = mongoose.model("event", eventSchema);
module.exports = event;