import {createSlice, PayloadAction, createAsyncThunk} from '@reduxjs/toolkit'
import EventService from '../services/eventService'


const initialState = {
    events: [],
    pageCount: 1,
    page: 1,
    categories: [],
    filterParams: {},
    isLoading: false
};


export const getEvents = createAsyncThunk('event/getEvents',
    async (params ) => {
        const response = await EventService.getEvents(params);
        return response.data
    }
)

export const getCategories = createAsyncThunk('event/getCategories',
    async (params ) => {
        const response = await EventService.getCategories()
        return response.data
    }
)

export const eventSlice = createSlice({
    name: 'event',
    initialState,
    reducers: {
        addEvent: (state, action) => {
            console.log(action.payload);
            action.payload.map((event) => state.events.push(event));
        },
        updatePage: (state, action) => {
            state.page = action.payload
        },
        updateFilters: (state, action) => {
            state.page = 1
            state.filterParams = action.payload;
        }
    },
    extraReducers: {
        [getEvents.pending]: (state, action) => {
            state.isLoading = true;
        },
        [getEvents.fulfilled]: (state, action) => {
            state.events = action.payload.events;
            state.pageCount = action.payload.pageCount;
            state.page = action.payload.page;
            state.isLoading = false;
        },
        [getCategories.fulfilled]: (state, action) => {
            state.categories = action.payload.categories;
        }
    }
})

// Action creators are generated for each case reducer function

export const  {addEvent, updatePage, updateFilters} = eventSlice.actions;
export default eventSlice.reducer;