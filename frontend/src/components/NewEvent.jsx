import React, {useState} from "react";

import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import EventService from "../services/eventService";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import TextField from "@mui/material/TextField";

import {AdapterDateFns} from "@mui/x-date-pickers/AdapterDateFns";
import {Button, Stack} from "@mui/material";
import {DesktopDatePicker} from "@mui/x-date-pickers/DesktopDatePicker";
import {LocalizationProvider} from "@mui/x-date-pickers/LocalizationProvider";
import {useNavigate} from "react-router-dom";
import {toast} from "react-toastify";
import {DateTimePicker} from "@mui/lab";

const NewEvent = () => {
    const navigate = useNavigate();
    const [eventValues, setEventValues] = useState({
        title: "",
        description: "",
        category: "",
        date: "",
        isVirtual: true,
        address: "",
    });

    // const handleChange = (newValue) => {
    //     if (new Date(newValue).toISOString().slice(0, 10) < currentDate) {
    //         toast.error("date must be a valid date");
    //     } else {
    //         setEventValues({...eventValues, date: newValue});
    //     }
    // };

    const onSubmit = async () => {
        try {
            const {title, description, category, date, isVirtual, address} = eventValues;
            await EventService.createEvent({
                title,
                description,
                category,
                date,
                isVirtual,
                address,
            });
            toast.success("Event created successfully")
            navigate("/");
        } catch (err) {
            toast.error(err.response.data.message);
        }
    };

    return (
        <div style={{paddingTop: "60px"}}>
            <Box
                boxShadow={6}
                sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    width: 600,
                    backgroundColor: "white",
                    margin: "70px auto",
                    "& > :not(style)": {m: 1},
                }}
            >
                <FormGroup sx={{width: "80%"}}>
                    <TextField
                        style={{margin: "10px 0"}}
                        id="demo-helper-text-misaligned-no-helper"
                        label="title"
                        fullwidth
                        value={eventValues.title}
                        onChange={(e) =>
                            setEventValues({...eventValues, title: e.target.value})
                        }
                    />
                    <TextField
                        style={{margin: "10px 0"}}
                        id="demo-helper-text-misaligned-no-helper"
                        label="category"
                        value={eventValues.category}
                        onChange={(e) =>
                            setEventValues({...eventValues, category: e.target.value})
                        }
                    />
                    <TextField
                        style={{margin: "10px 0"}}
                        id="demo-helper-text-misaligned-no-helper"
                        label="description"
                        value={eventValues.description}
                        multiline
                        minRows={2}
                        maxRows={4}
                        onChange={(e) =>
                            setEventValues({...eventValues, description: e.target.value})
                        }
                    />
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <Stack spacing={3}>
                            <DateTimePicker
                                style={{margin: "10px 0"}}
                                label="Date"
                                disablePast
                                // inputFormat="MM/dd/yyyy"
                                value={eventValues.date}
                                onChange={(nv) => setEventValues({...eventValues, date: nv})}
                                renderInput={(params) => <TextField {...params} />}
                            />
                        </Stack>
                    </LocalizationProvider>
                    <FormControlLabel
                        control={
                            <Checkbox
                                defaultChecked
                                onChange={(e) =>
                                    setEventValues({
                                        ...eventValues,
                                        isVirtual: e.target.checked,
                                    })
                                }
                            />
                        }
                        label="Virtual"
                    />
                    <TextField
                        style={{margin: "10px 0"}}
                        id="demo-helper-text-misaligned-no-helper"
                        label="address"
                        value={eventValues.address}
                        onChange={(e) =>
                            setEventValues({...eventValues, address: e.target.value})
                        }
                    />
                    <Button
                        variant="contained"
                        style={{margin: "10px 0", backgroundColor: "Black"}}
                        onClick={() => {
                            onSubmit();
                        }}
                    >
                        Submit
                    </Button>
                </FormGroup>
            </Box>
        </div>
    );
};

export default NewEvent;
