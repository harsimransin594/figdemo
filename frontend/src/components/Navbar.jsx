import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { useNavigate } from "react-router-dom";
import AddIcon from '@mui/icons-material/Add';
export default function Navbar() {
  const navigate = useNavigate();
  return (
    <Box sx={{ flexGrow: 1, position:"fixed", top:"0" , width:"100%", zIndex:"100"}}>
      <AppBar position="static" sx={{ bgcolor: "black" }}>
        <Toolbar>
          <Typography variant="h6" component="div">
            <Button color="inherit" onClick={() => navigate("/")}>
              Home
            </Button>
          </Typography>
          <Button color="primary" onClick={() => navigate("/create")} endIcon={<AddIcon/>}>
            Create
          </Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
