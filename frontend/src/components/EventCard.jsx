import * as React from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CategoryOutlinedIcon from "@mui/icons-material/CategoryOutlined";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import StreamSharpIcon from '@mui/icons-material/StreamSharp';
import {Chip, IconButton, Stack, Tooltip} from "@mui/material";

const bull = (
    <Box
        component="span"
        sx={{display: "inline-block", mx: "2px", transform: "scale(0.8)"}}
    >
        •
    </Box>
);

export default function EventCard({event}) {
    const colorArr = ["#4C3A51", "#1F4690", "#4D4C7D", "#371B58", "#5F7161"]
    const selectedColor = Math.floor(Math.random() * 5);
    let date = new Date(event.date);
    const day = date.toLocaleDateString('en-us', {weekday: 'long'})
    const date1 = date.toLocaleString();

    return (
        <Card sx={{width: 1000, margin: "30px"}}>
            <CardContent style={{padding: 0}}>
                <div className="date_Container" style={{minHeight: "18vh", backgroundColor: colorArr[selectedColor]}}>
                    <Typography
                        sx={{
                            fontSize: 24,
                            height: "30%",
                            paddingTop: "5px",
                            fontWeight: "600",
                            color: "white"
                        }}
                        color="text.secondary"
                        gutterBottom
                    >
                        {day}
                    </Typography>
                    <Typography
                        sx={{
                            fontSize: 14,
                            height: "30%",
                            color: "white",
                        }}
                        color="text.secondary"
                        gutterBottom
                    >
                        {date1}
                    </Typography>
                </div>
                <div className="content_container" style={{
                    height: "60%",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "flex-start",
                    padding: "8px"
                }}>
                    <Typography
                        variant="h5"
                        component="div"
                        style={{
                            margin: "2px",
                            fontSize: "20px",
                            fontWeight: "600"
                        }}
                    >
                        {event.title}
                    </Typography>
                    <Typography sx={{mb: 1.5, textAlign: "left"}} color="text.secondary">
                        {event.description}
                    </Typography>
                    <Typography
                        color="text.secondary"
                        sx={{
                            mb: 1.5,
                            fontSize: "14px",
                            display: "flex",
                            justifyContent: "center",
                        }}
                    >
                        <Stack direction="row" spacing={2}>

                            <LocationOnIcon style={{marginRight: "2px"}} fontSize="small"/>
                            {event.address}
                        </Stack>

                    </Typography>
                    <Typography
                        color="text.secondary"
                        sx={{
                            mb: 1.5,
                            display: "flex",
                            justifyContent: "center",
                        }}
                    >
                        {/*<CategoryOutlinedIcon style={{marginRight: "2px"}} fontSize="small"/>*/}
                        Category &nbsp;<Chip label={event.category} size="small" />
                    </Typography>

                    <Typography sx={{
                        mb: 1.5,
                        display: "flex",
                        justifyContent: "center",
                    }}>
                        {event.isVirtual && <Tooltip title={"This is a virtual event"}>
                            <IconButton>
                                <StreamSharpIcon/>
                            </IconButton>
                        </Tooltip>}
                    </Typography>

                </div>
            </CardContent>
        </Card>
    )
}
