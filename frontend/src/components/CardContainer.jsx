import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import EventCard from "./EventCard";
import Filter from "./Filter";

import {getEvents, updatePage, getCategories} from '../redux/eventSlice'
import React, {useEffect} from "react";
import {CircularProgress, Pagination, Stack} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";

const CardContainer = () => {

    const {events, pageCount, page, filterParams, isLoading} = useSelector((state) => state.event);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getEvents({page, ...filterParams}))
    }, [page, filterParams]);

    return (
        <>
            <Filter/>
            { isLoading ? <CircularProgress/>:
                <>
                    <div
                        className="CardContainer"
                        style={{
                            display: "flex",
                            flexWrap: "wrap",
                            justifyContent: "center",
                            padding: "0 200px",
                        }}
                    >
                        {!isLoading && events.length > 0 && events.map((event) => (
                            <EventCard event={event} key={event._id}/>
                        ))}
                        {!isLoading && events.length === 0 && <>
                            <Stack sx={{width: '100%'}} spacing={2}>
                                <Alert severity="warning">
                                    <AlertTitle>Sorry</AlertTitle>
                                    <strong> There are no events available!</strong>
                                </Alert>
                            </Stack>
                        </>}

                    </div>
                    {events.length > 0 && <Stack spacing={2} style={{alignItems: "center", padding: "2rem"}}>
                        <Pagination
                            count={pageCount}
                            shape="rounded"
                            page={page}
                            onChange={(_, page) => dispatch(updatePage(page))}
                        />
                    </Stack>}
                </>
            }
        </>
    );
};

export default CardContainer;
