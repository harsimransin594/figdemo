import * as React from "react";
import {useEffect, useState} from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import {useDispatch, useSelector} from "react-redux";
import {updateFilters} from "../redux/eventSlice";
import SendIcon from '@mui/icons-material/Send';
import {Stack} from "@mui/material";
import ClearAllIcon from '@mui/icons-material/ClearAll';


const ariaLabel = {"aria-label": "description"};

export default function Filter() {
    const dispatch = useDispatch();
    const {categories, filterParams} = useSelector(state => state.event);
    const [title, setTitle] = useState("")
    const [category, setCategory] = useState("")
    const [address, setAddress] = useState("")
    const [isVirtual, setIsVirtual] = useState("null")

    useEffect(()=> {
        const {title="", category="", isVirtual="", address=""} = filterParams;
        setTitle(title);
        setCategory(category)
        setAddress(address);
        setIsVirtual(isVirtual);
    }, [filterParams])

    const handleSubmit = () => {
        dispatch(updateFilters({title, category, isVirtual: isVirtual === "null" ? undefined : isVirtual, address}))
    }
    const handleClearAll = () =>{
        dispatch(updateFilters({}));
    }

    return (
        <Box
        >
            <div className={"filter-component-inputs"}>
                <FormControl  sx={{m: 2, minWidth: 120}}>
                    <TextField
                        label="Search by Title"
                        id="outlined-size-small"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                    />
                </FormControl>
                <FormControl  sx={{m: 2, minWidth: 120}}>
                    <TextField
                        label="Search by Address"
                        id="outlined-size-small"
                        value={address}
                        onChange={(e) => setAddress(e.target.value)}
                    />
                </FormControl>
                <FormControl sx={{m: 2, minWidth: 120}}>
                    <InputLabel id="demo-simple-select-label">
                        Category
                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={category}
                        label="category"
                        onChange={(e) => setCategory(e.target.value)}
                    >
                        {
                            categories.map((category) => (
                                <MenuItem key={category} value={category}>{category}</MenuItem>))
                        }

                    </Select>
                </FormControl>
                <FormControl sx={{m: 2, minWidth: 120}}>
                    <InputLabel id="demo-simple-select-label">
                        Mode
                    </InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={isVirtual}
                        label="is_virtual"
                        onChange={(e) => setIsVirtual(e.target.value)}
                    >
                        <MenuItem value={true}>Virtual</MenuItem>
                        <MenuItem value={false}>Physical</MenuItem>
                    </Select>
                </FormControl>
            </div>

            <Stack className={"filter-component-inputs"} direction="row" spacing={2}>
                <Button variant="contained" endIcon={<SendIcon/>} onClick={handleSubmit}>
                    Submit
                </Button>
                <Button endIcon={<ClearAllIcon/>} onClick={handleClearAll}>
                    Clear All
                </Button>
            </Stack>

        </Box>
    );
}
