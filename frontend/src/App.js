import './App.css';
import CardContainer from './components/CardContainer';
import Filter from "./components/Filter";
import Navbar from './components/Navbar';
import NewEvent from "./components/NewEvent";
import {Routes, Route, BrowserRouter} from "react-router-dom";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {getCategories} from "./redux/eventSlice";
import {Provider, useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {store} from "./redux/store";

function App() {
    const dispatch = useDispatch();
    useEffect(() => {
        console.log("Hello")
        dispatch(getCategories());
    }, []);

    return (
        <BrowserRouter>
            <Navbar/>
            <div className="App">
                <Routes>
                    <Route path="/" element={<CardContainer/>}/>
                    <Route path="/create" element={<NewEvent/>}/>
                </Routes>
                <ToastContainer/>
            </div>
        </BrowserRouter>

    );
}

export default App;
