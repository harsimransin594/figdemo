import axios from "axios";

const authorization = `Bearer ${process.env.REACT_APP_AUTH_TOKEN}`;
const baseUrl = process.env.REACT_APP_BASE_URL;
console.log({baseUrl, authorization})
export default class EventService {

    static createEvent = async ({title, description, category, date, isVirtual, address}) => {
        const res = await axios.post(`${baseUrl}`, {
            title, description, category, date, isVirtual, address
        }, {
            headers: {
                authorization
            }
        })
        return res.data;
    }

    static getEvents = async (params) => {
        for (let key in params) {
            if (params[key] === undefined || params[key] === "") {
                delete params[key];
            }
        }
        let queryString = new URLSearchParams(params).toString();
        const res = await axios.get(`${baseUrl}?${queryString}`, {
            headers:{
                authorization
            }
        })
        return res.data;
    }

    static getCategories = async () => {
        const res = await axios.get(`${baseUrl}/category`, {
            headers:{
                authorization
            }
        })
        return res.data;
    }

}
